package fr.isika.al12.footballstats.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isika.al12.footballstats.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>{
	
	User findByEmail(String mail);
}
