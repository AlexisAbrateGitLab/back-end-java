package fr.isika.al12.footballstats.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.isika.al12.footballstats.dao.UserRepository;
import fr.isika.al12.footballstats.model.User;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	private UserRepository userRepo;
	
	@PostMapping("/login")
	public ResponseEntity<?> loginUser(@RequestBody User userData){
		System.out.println(userData);
		User user = userRepo.findByEmail(userData.getEmail());
		if(user.getPassword().equals(userData.getPassword())) {
			return ResponseEntity.ok(user);
		}
		return ResponseEntity.badRequest().body(new String("Wrong"));
		
	}
	
	@PostMapping("/inscription")
	public ResponseEntity<?> inscriptionUser(@RequestBody User userData) {
		userRepo.save(userData);
		return ResponseEntity.ok(userData);
	}
}
